const iterable = {
	[Symbol.iterator](){
		let step = 0;
		const iterator = {
			next(){
				step++;
				if(step === 1){
					return {value: 'this ',done: true};
				}
				else if(step === 2){
					return {value: 'is ',done: true};
				}
				else if(step === 3){
					return {value: 'iterable',done: true};
				}

				return {value: 'undefined',done: false};
			}
		}
		return iterator;
	}
}
var iterator = iterable[Symbol.iterator]();
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());



/*
	const array = ['a', 'b', 'c', 'd', 'e'];
	const newArray = [1, ...array, 2, 3];

	it's solve with iterator

	const output array is = [1,'a', 'b', 'c', 'd', 'e',2,3];
*/


const array = ['a', 'b', 'c', 'd', 'e'];
const iterator2 = array[Symbol.iterator]();
const newArray = [1];
for (let nextValue = iterator2.next(); nextValue.done !== true; nextValue = iterator2.next()) {
  newArray.push(nextValue.value);
};
newArray.push(2);
newArray.push(3);

console.log(newArray);