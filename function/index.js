// constructor without class and parameter
let foo = new Function("return 'Call from Foo'");
console.log(foo());


// constructor with class
class Person {
    constructor() {
        console.log("Call from Person");
    }
}
const person = new Person();

//anonymos function

// with params
let func = (function(...params){
	console.log("Call from anonymos function with parameter");
	console.log(params);
});
func(12,25,23);

//without params

(function(){
	console.log("Call from anonymos function without params");
})();


//Arow function

//variation 1
var arrofunc = () => {
	console.log("Call From Arrow Function variation 1");
}
arrofunc();

//variation 2
arrofunc = () => "Call From Arrow Function variation 2";
console.log(arrofunc());

//with parameter

arrofunc = (x,y,z) => x+y+z;
console.log(arrofunc(10,10,10));

//with spread/rest operator

arrofunc = (...params) => params;
console.log(arrofunc(10,10,15));

//generator function

function* query(){
	const username = yield;
	if(username =='admin'){
		return 'Authorized logged in';
	}
	else{
		return 'Unauthorized access';
	}
}
const instance = query();
console.log(instance.next());
console.log(instance.next('admin'));

function * generatorFunction() { // Line 1
  console.log('This will be executed first.');
  yield 'Hello, ';   // Line 2  console.log('I will be printed after the pause');  
  yield 'World!';
}
const generatorObject = generatorFunction(); // Line 3console.log(generatorObject.next().value); // Line 4
console.log(generatorObject.next()); // Line 5
// console.log(generatorObject.next().value); // Line 6

