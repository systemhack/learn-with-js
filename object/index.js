// Object declaration

//declaration way 1
// function ProgrammingLanguage(name,type,level){
// 	this.name = name;
// 	this.type = type;
// 	this.level = level;
// }

// var c = new ProgrammingLanguage('C Programming',"Core",'low');

// console.log('Language: '+c.name);
// console.log('Type: '+c.type);
// console.log('Level: '+c.level);

// declaration way 2

// var ProgrammingLanguage = {
// 	name: '',
// 	type: '',
// 	level: '',
// 	display: function(){
// 		return 'Name: '+this.name+' Type: '+this.type+' level: '+this.level;
// 	}
// }
// console.log(typeof(ProgrammingLanguage));
// var c = Object.create(ProgrammingLanguage);

// c.name = "C Programming";
// c.type = "Core";
// c.level = "Low";
// console.log(c.display());

//Defining properties for an object type

// function ProgrammingLanguage(name,type,level){
// 	this.name = name;
// 	this.type = type;
// 	this.level = level;
// }
// var c = new ProgrammingLanguage('C Programming',"Core",'low');

// console.log('Language: '+c.name);
// console.log('Type: '+c.type);
// console.log('Level: '+c.level);

// ProgrammingLanguage.prototype.inventor = null;

// var c = new ProgrammingLanguage('C Programming',"Core",'low');
// c.inventor = 'Ritche';
// console.log("=======================");
// console.log('Language: '+c.name);
// console.log('Type: '+c.type);
// console.log('Level: '+c.level);
// console.log('inventor: '+c.inventor);

// deleting  object property

// function ProgrammingLanguage(name,type,level){
// 	this.name = name;
// 	this.type = type;
// 	this.level = level;
// }

// var c = new ProgrammingLanguage('C Programming',"Core",'low');

// console.log('Language: '+c.name);
// console.log('Type: '+c.type);
// console.log('Level: '+c.level);

// delete c.level
// console.log("=======================");
// console.log('level' in c);

//Comparing objects

// Two variables, two distinct objects with the same properties
var fruit = {name: 'apple'};
var fruitbear = {name: 'apple'};

console.log("Assigned value")

console.log(fruit); 
console.log(fruitbear); 

console.log("Comparing..")

console.log(fruit == fruitbear); // return false
console.log(fruit === fruitbear); // return false

// Two variables, a single object
var fruit = {name: 'apple'};
var fruitbear = fruit;  // Assign fruit object reference to fruitbear

console.log("Assign fruit object reference to fruitbear");

// Here fruit and fruitbear are pointing to same object
console.log(fruit == fruitbear); // return true
console.log(fruit === fruitbear); // return true

fruit.name = 'grape';

console.log("fruit.name = 'grape'");

console.log(fruit); 
console.log(fruitbear); 


console.log(fruit == fruitbear);
console.log(fruit === fruitbear); 


